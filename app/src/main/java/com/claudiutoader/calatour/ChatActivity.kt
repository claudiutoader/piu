package com.claudiutoader.calatour

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.claudiutoader.calatour.adapter.ChatRecyclerViewAdapter
import com.claudiutoader.calatour.model.Message
import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class ChatActivity : AppCompatActivity(), View.OnClickListener {
    private val dataSource = ArrayList<Message>()
    private var count: Int = 1
    private lateinit var usernameValue: String

    private lateinit var chatAdapter: ChatRecyclerViewAdapter
    private lateinit var sendButton: Button
    private lateinit var message: EditText
    private lateinit var recyclerView: RecyclerView
    private lateinit var username: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        val sharedPref = this.getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE) ?: return
        usernameValue = sharedPref.getString(getString(R.string.username_key), "null").toString()

        message = findViewById(R.id.message_edit_text)
        sendButton = findViewById(R.id.send_message_button)
        recyclerView = findViewById(R.id.chat_recycler_view)
        username = findViewById(R.id.username_text_view)

        username.text = usernameValue
        sendButton.setOnClickListener(this)
        chatAdapter = ChatRecyclerViewAdapter(this, dataSource)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = chatAdapter

        Handler(Looper.getMainLooper()).post(object : Runnable {
            override fun run() {
                automatedMessage()
                Handler(Looper.getMainLooper()).postDelayed(this, 5000)
            }
        })
    }

    fun automatedMessage() {
        dataSource.add(0,
            Message(
                "Computer",
                "Automated message: " + count,
                DateTimeFormatter
                    .ofPattern("yyyy-MM-dd HH:mm:ss")
                    .withZone(ZoneOffset.UTC)
                    .format(Instant.now()),
                false
            )
        )
        chatAdapter.notifyItemInserted(0)
        count ++
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        getMenuInflater().inflate ( R.menu.chat_menu, menu )
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.design1) {
            chatAdapter.design = 1
        }
        else if (item.itemId == R.id.design2) {
            chatAdapter.design = 2
        }
        else if (item.itemId == R.id.both_designs) {
            chatAdapter.design = 3
        }

        chatAdapter.notifyDataSetChanged()
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {

        dataSource.add(0,
            Message(
                usernameValue,
                message.text.toString(),
                DateTimeFormatter
                    .ofPattern("yyyy-MM-dd HH:mm:ss")
                    .withZone(ZoneOffset.UTC)
                    .format(Instant.now()),
                false
            )
        )
        chatAdapter.notifyItemInserted(0)
        message.text.clear()
    }
}