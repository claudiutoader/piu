package com.claudiutoader.calatour

import android.content.Context
import android.content.Intent
import android.graphics.Color.red
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.ContextCompat

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val signInButton = findViewById<Button>(R.id.signInButton)

        signInButton.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        val username = findViewById<EditText>(R.id.editTextUsername)
        val password = findViewById<EditText>(R.id.editTextPassword)
        val loginTextView = findViewById<TextView>(R.id.login_text_view)

        var mUsername = username.text.toString()
        var mPassword = password.text.toString()

        var usernameError = findViewById<TextView>(R.id.textViewErrorUsername)
        var passwordError = findViewById<TextView>(R.id.textViewErrorPassword)


        if (TextUtils.isEmpty(mUsername)) {
            username.error = "Username is required."
            return;
        }

        if (TextUtils.isEmpty(mPassword)) {
            password.error = "Password is required."
            return;
        }

        if (mUsername.length < 3) {
            username.error = "Username must be at least 3 characters."
            return
        }

        if (mPassword.length < 6) {
            password.error = "Password must be at least 6 characters."
            return
        }

        if (mUsername == "admin" && mPassword == "password") {
            val sharedPref = this.getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE) ?: return
            with (sharedPref.edit()) {
                putString(getString(R.string.username_key), mUsername)
                apply()
            }

            loginTextView.text = "Login successful!"
            loginTextView.setTextColor(ContextCompat.getColor(this, R.color.green));
            val intent = Intent(this, OffersActivity::class.java)
            startActivity(intent)

        } else {
            loginTextView.text = "Incorrect username or password"
            loginTextView.setTextColor(ContextCompat.getColor(this, R.color.red));
        }

    }
}