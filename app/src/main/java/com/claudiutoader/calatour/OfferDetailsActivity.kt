package com.claudiutoader.calatour

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

class OfferDetailsActivity : AppCompatActivity() {
    private var isFavorite = false
    private var offerPosition = -1

    companion object {
        var count = 1
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offer_details)

        val titleTextView = findViewById<TextView>(R.id.offer_title)
        val offerImage = findViewById<ImageView>(R.id.offer_image)
        val priceTextView = findViewById<TextView>(R.id.offer_price)
        val descriptionTextView = findViewById<TextView>(R.id.offer_description)
        val countTextView = findViewById<TextView>(R.id.details_count)


        val title = intent.getStringExtra("offerTitle")
        val image = intent.getIntExtra("offerImage", 0)
        val description = intent.getIntExtra("offerDescription", 0)
        val price = intent.getIntExtra("offerPrice", 0)
        val favorite = intent.getBooleanExtra("offerFavorite", false)
        val position = intent.getIntExtra("offerPosition", -1)

        isFavorite = favorite
        offerPosition = position

        titleTextView.text = title
        offerImage.setImageDrawable(getDrawable(image))
        priceTextView.text = (price.toString() + " EUR")
        descriptionTextView.text = resources.getString(description)
        countTextView.text = "Details page displayed: " + count

    }


    override fun onCreateOptionsMenu ( menu: Menu? ):Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate ( R.menu.offer_details_option_menu, menu )
        // change programmatically some menu items

        if (isFavorite) {
            menu!!.getItem(0).title = "REMOVE FROM FAVORITE"
        }
        else {
            menu!!.getItem(0).title = "ADD TO FAVORITE"
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val textView = findViewById<TextView>(R.id.favorites_button_id)

        if (item.itemId == R.id.favorites_button_id) {
            if (isFavorite) {
                textView.text = "ADD TO FAVORITES"
                isFavorite = false
                Toast.makeText(applicationContext, "Removed from favorites", Toast.LENGTH_LONG).show()
            }
            else {
                textView.text = "REMOVE FROM FAVORITES"
                isFavorite = true
                Toast.makeText(applicationContext, "Added to favorites", Toast.LENGTH_SHORT).show()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        count ++
        val returnIntent = Intent()
        returnIntent.putExtra ( "offerIsFavoriteFromChild", isFavorite )
        returnIntent.putExtra ( "offerPositionFromChild", offerPosition )
        setResult ( Activity.RESULT_OK, returnIntent )
        finish()
    }
}