package com.claudiutoader.calatour

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.claudiutoader.calatour.adapter.OffersAdapter
import com.claudiutoader.calatour.model.Offer
import com.claudiutoader.calatour.model.PopulateOffersList

class OffersActivity : AppCompatActivity() {
    private val dataSource = ArrayList<Offer>()
    private lateinit var offersAdapter: OffersAdapter
    private val offerDetailsId = 1015

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offers)

        val offersListView = findViewById<ListView>(R.id.offers_list_view)
        val progressBar = findViewById<ProgressBar>(R.id.progress_bar_id)

        offersListView.visibility = View.GONE

        val runnable = Runnable { offersListView.visibility = View.VISIBLE
                                  progressBar.visibility = View.GONE}

        Handler(Looper.getMainLooper()).postDelayed(runnable, 3000)

        PopulateOffersList.getOffers(dataSource)


        offersAdapter = OffersAdapter(this, dataSource)
        offersListView.adapter = offersAdapter

        registerForContextMenu(offersListView)


        offersListView.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(this, OfferDetailsActivity::class.java)
            intent.putExtra("offerTitle", dataSource[position].title)
            intent.putExtra("offerImage", dataSource[position].image)
            intent.putExtra("offerDescription", dataSource[position].description)
            intent.putExtra("offerPrice", dataSource[position].price)
            intent.putExtra("offerFavorite", dataSource[position].isFavorite)
            intent.putExtra("offerPosition", position)

            startActivityForResult(intent, offerDetailsId)
        }

    }


    override fun onActivityResult ( requestCode: Int, resultCode: Int, data: Intent? )
    {
        super.onActivityResult ( requestCode, resultCode, data )
        if ( requestCode == offerDetailsId )
        {
            if ( resultCode == Activity.RESULT_OK )
            {
                // extragerea informațiilor din Intent
                val result = data?.getBooleanExtra ( "offerIsFavoriteFromChild", false )
                val childPosition = data?.getIntExtra("offerPositionFromChild", -1);

                if (childPosition == -1) {
                    Toast.makeText(applicationContext, "Offer not updated", Toast.LENGTH_SHORT).show()
                }
                else {
                    dataSource[childPosition!!].isFavorite = result
                }
            }
            if ( resultCode == Activity.RESULT_CANCELED )
            {
                // codul executat dacă activitatea copil nu a returnat nici un rezultat
            }
        }
    }


    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)

        if (v!!.id == R.id.offers_list_view) {
            val info = menuInfo as AdapterView.AdapterContextMenuInfo
            menu!!.setHeaderTitle(dataSource[info.position].title)
            menuInflater.inflate(R.menu.offer_context_menu, menu)
        }
    }


    override fun onCreateOptionsMenu ( menu: Menu? ):Boolean
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate ( R.menu.offers_list_options_menu, menu )
        // change programmatically some menu items
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected ( item: MenuItem): Boolean {
        // Gestionați aici interacțiunea utilizatorului cu bara de opțiuni. Interacțiunea
        // cu butonul Home/Up va fi în mod implicit gestionată de către bara de opțiuni
        // dacă pentru activitatea curentă există mențiunea activității părinte în fișierul
        // AndroidManifest.xml.

        if ( item.itemId == R.id.sign_out_button)
        {
            // cod pentru Oțiunea1
            return true
        } else if (item.itemId == R.id.menu_reset) {
            PopulateOffersList.getOffers(dataSource)
            offersAdapter.notifyDataSetChanged()
            Toast.makeText(applicationContext, "List has been reset!", Toast.LENGTH_LONG).show()


        } else if (item.itemId == R.id.menu_clear) {
            for (offer: Offer in dataSource) {
                offer.isFavorite = false
            }
            offersAdapter.notifyDataSetChanged()
        } else if (item.itemId == R.id.menu_chat) {
            val intent = Intent(this, ChatActivity::class.java)
            startActivity(intent)
        }

        return super.onOptionsItemSelected ( item );
    }

    val goBackToLogin = {
        dialog:DialogInterface, whitch: Int->
        finish()
    }

    override fun onBackPressed() {
        val builder = AlertDialog.Builder ( this )
        builder.setTitle ( " Confirmation " )
            .setMessage ( " Please confirm logout intentions " )
            .setPositiveButton ( "Confirm", DialogInterface.OnClickListener(function=goBackToLogin))
            .setNegativeButton ( "Cancel", null)
        builder.create().show()
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val menuInfo = item.menuInfo as AdapterView.AdapterContextMenuInfo

        if (item.itemId == R.id.menu_remove_id) {
            offersAdapter.removeItem(dataSource[menuInfo.position])
        }
        else if (item.itemId == R.id.menu_add_id) {
            offersAdapter.addItem(menuInfo.position, Offer("Title added", R.drawable.offer_1, 0, R.string.new_offer_description))
        }


        offersAdapter.notifyDataSetChanged()
        return super.onContextItemSelected(item)
    }
}