package com.claudiutoader.calatour.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.claudiutoader.calatour.model.Message
import com.claudiutoader.calatour.R
import com.claudiutoader.calatour.viewholder.ChatViewHolder

class ChatRecyclerViewAdapter(private val context: Context, private val dataSource: ArrayList<Message>):
    RecyclerView.Adapter<ChatViewHolder>() {

    var design: Int = 1
    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        val view = inflater.inflate ( viewType, parent, false )
        return ChatViewHolder (view, viewType)
    }

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        holder.bindData(dataSource.get(position))
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun getItemViewType(position: Int): Int {
        if (design == 1) return R.layout.chat_item_1
        if (design == 2) return R.layout.chat_item_2

        return if (position % 2 == 0) R.layout.chat_item_2
        else R.layout.chat_item_1
    }
}