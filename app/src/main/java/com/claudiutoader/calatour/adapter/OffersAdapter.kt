package com.claudiutoader.calatour.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.claudiutoader.calatour.model.Offer
import com.claudiutoader.calatour.R

class OffersAdapter(private val context: Context, private val dataSource: ArrayList<Offer>): BaseAdapter() {
    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.offer_card, parent, false)

        val titleTextView = rowView.findViewById<TextView>(R.id.offer_title)
        val offerImage = rowView.findViewById<ImageView>(R.id.offer_image)
        val priceTextView = rowView.findViewById<TextView>(R.id.offer_price)
        val descriptionTextView = rowView.findViewById<TextView>(R.id.offer_description)

        val offer: Offer = dataSource[position]
        titleTextView.text = offer.title
        offerImage.setImageDrawable(context.getDrawable(offer.image))
        priceTextView.text = (offer.price.toString() + " EUR")
        descriptionTextView.text = context.resources.getString(offer.description)


        return rowView
    }

    fun addItem(position: Int, item: Offer) {
        dataSource.add(position, item)
    }

    fun removeItem(item: Offer){
        dataSource.remove(item)
    }

}