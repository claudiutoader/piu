package com.claudiutoader.calatour.model

import com.claudiutoader.calatour.R

class PopulateOffersList {

    companion object Factory {
        fun getOffers(dataSource: ArrayList<Offer>): ArrayList<Offer> {
            dataSource.clear()
            dataSource.add(
                Offer(
                    "Barcelona, 3 nights",
                    R.drawable.offer_1,
                    300,
                    R.string.offer_1_description
                )
            );
            dataSource.add(
                Offer(
                    "Maldive, 7 nights",
                    R.drawable.offer_2,
                    1050,
                    R.string.offer_2_description
                )
            )
            dataSource.add(
                Offer(
                    "Thailand, 10 nights",
                    R.drawable.offer_3,
                    1250,
                    R.string.offer_3_description
                )
            )

            return dataSource
        }
    }
}