package com.claudiutoader.calatour.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.claudiutoader.calatour.model.Message
import com.claudiutoader.calatour.R

class ChatViewHolder (private val view: View, private val design: Int) : RecyclerView.ViewHolder(view), View.OnClickListener{

    private lateinit var message: Message

    private var usernameRef: TextView
    private var messageRef: TextView
    private var dateAndTimeRef: TextView
    private var image: ImageView

    init {

        if (design == R.layout.chat_item_1) {
            usernameRef = view.findViewById(R.id.username_text_view_1)
            messageRef = view.findViewById(R.id.message_text_view_1)
            dateAndTimeRef = view.findViewById(R.id.date_text_view_1)
            image = view.findViewById(R.id.star_image_view_1)
        }
        else {
            usernameRef = view.findViewById(R.id.username_text_view_2)
            messageRef = view.findViewById(R.id.message_text_view_2)
            dateAndTimeRef = view.findViewById(R.id.date_text_view_2)
            image = view.findViewById(R.id.star_image_view_2)
        }
        image.visibility = View.INVISIBLE
        view.setOnClickListener(this)
    }

    fun bindData(m: Message) {
        message = m

        usernameRef.text = m.username
        messageRef.text = m.message
        dateAndTimeRef.text = m.date

        image.visibility = if (!m.clicked){
            View.INVISIBLE
        } else{
            View.VISIBLE
        }
    }

    override fun onClick(v: View?) {
        message.clicked = !message.clicked

        image.visibility = if (!message.clicked){
            View.INVISIBLE
        } else{
            View.VISIBLE
        }
    }

}